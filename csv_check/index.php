<?php

$name		= $argv[1];

if(file_exists($name))
{
	$contents	= file_get_contents($name);
	$lines		= explode("\n", $contents);
	
	$text		= "A-Za-z,' !@#$%\^&*\(\)";
	
	$headers	= 1;
	$pattern	= "/^\"([0-9]{8})\"\,\"([a-zA-Z 0-9\':#*+-@])\"\,\"/";
	$pattern	= "/^(\"){0,1}[0-9]{8}(\"){0,1}\,(\"){0,1}[$text]/";
	
	echo "\n\n";
	echo $pattern;
	echo "\n\n";
	
	//	"20161019","3205 UT Brzk UTRECHT NLD","NL92INGB0008863097","","BA","Af","2,50","Betaalautomaat","Pasvolgnr:011 19-10-2016 09:04 Transactie:A3V991 Term:FD2M52"
	
	foreach($lines as $i => $line)
	{
		$line	= '"20161019","3"';
		
		if($i >= $headers && $line != "")
		{
			$check		= preg_match($pattern, $line);
			
			if(!$check)
			{
				echo "\n\n";
				echo "ERROR ON LINE " . $i;
				echo "\n\n";
				echo $line;
				echo "\n\n";
				exit();
			}
		}
	}
	
	echo "File passed";
	echo "\n";
	
}

?>