<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
		<script>
			$( function() {
				$("[name=amount],[name=price],[name=vat]").change(updateTotal);
				
				$("[name=go]").click(function()
				{
					var new_total	= $("[name=omrekenen]").val();
					
					
					
					var old_total		= 0;
					
					$("tr.row").each(function(index, el)
					{
						var row			= $(el);
						var amount		= row.find("[name=amount]").val();
						var price		= row.find("[name=price]").val();
						var vat			= row.find("[name=vat]").val();
						
						
						var line_total	= (amount * price);
						old_total		+= line_total * (1 + vat * 0.01);
					});
					
					var r			= new_total / old_total;
					
					$("tr.row").each(function(index, el)
					{
						var row			= $(el);
						var amount		= row.find("[name=amount]").val();
						var price		= row.find("[name=price]").val();
						var vat			= row.find("[name=vat]").val();
						var new_price	= price * r;
						
						row.find("[name=price]").val(Math.round(new_price*100)/100);
					});
					
					updateTotal();
				});
				updateTotal();
			} );
			
			function updateTotal()
			{
				var total_vat	= 0;
				var total		= 0;
				
				$("tr.row").each(function(index, el)
				{
					var row			= $(el);
					var amount		= row.find("[name=amount]").val();
					var price		= row.find("[name=price]").val();
					var vat			= row.find("[name=vat]").val();
					
					
					var line_total	= (amount * price);
					total_vat		+= line_total * (vat * 0.01);
					total			+= line_total * (1 + vat * 0.01);
					row.find("[name=line_total]").val(line_total);
				});
				$("[name=total_vat]").val(Math.round(total_vat*100)/100);
				$("[name=total]").val(Math.round(total*100)/100);
			}
		</script>
	</head>
	<body>
		
		<table>
			<thead>
				<td>Omschrijving</td>
				<td>Aantal</td>
				<td>Prijs</td>
				<td>BTW</td>
				<td>Regel totaal</td>
			</thead>
			<tr class="row">
				<td>Factuurregel 1</td>
				<td><input type="number" name="amount" value="1"/></td>
				<td><input type="number" name="price" value="10"/></td>
				<td><input type="number" name="vat" value="21"/></td>
				<td><input type="number" name="line_total" disabled="disabled" /></td>
			</tr>
			<tr class="row">
				<td>Factuurregel 2</td>
				<td><input type="number" name="amount" value="1"/></td>
				<td><input type="number" name="price" value="10"/></td>
				<td><input type="number" name="vat" value="21"/></td>
				<td><input type="number" name="line_total" disabled="disabled" /></td>
			</tr>
			<tr>
				<td colspan="4">VAT</td>
				<td><input type="number" name="total_vat" disabled="disabled"/></td>
			</tr>
			<tr>
				<td colspan="4">Totaal</td>
				<td><input type="number" name="total" disabled="disabled"/></td>
			</tr>
		</table>
		
		<br/>
		
		Transactie totaal:
		<br/>
		<input type="number" name="omrekenen" value="11.93"/><input type="button" value="reken om!" name="go"/>
		
	</body>
</html>