<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Draggable + Sortable</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
  ul { list-style-type: none; margin: 0; padding: 0; margin-bottom: 10px; }
  li { margin: 5px; padding: 5px; width: 150px; }
  .ui-state-default, .ui-state-highlight { display: inline-block;}
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable" ).sortable({
      revert: true
    });
    $( ".draggable" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
    $( "ul, li" ).disableSelection();
  } );
  </script>
</head>
<body>
 
<ul id="sortable">
  <li class="ui-state-default">001</li>
</ul>
<br/>
<br/>
<br/>
<br/>
<ul>
 <li id="draggable" class="draggable ui-state-highlight">2016</li>
 <li id="draggable" class="draggable ui-state-highlight">09</li>
 <li id="draggable" class="draggable ui-state-highlight">001</li>
 <li id="draggable" class="draggable ui-state-highlight"><input type="text"/></li>
</ul>


 
 
</body>
</html>