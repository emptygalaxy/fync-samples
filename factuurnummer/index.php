<?php
session_start();

$template	= "INV-%IIII";

#$year_index		= 200;
if(isset($_SESSION["index"]))	$year_index		= $_SESSION["index"] ++;
else 							$year_index	= $_SESSION["index"]	= 1;

$invoice_date		= time();

$num		= $template;
$num		= str_ireplace("%YYYY", date("Y", $invoice_date), $num);
$num		= str_ireplace("%YY", date("y", $invoice_date), $num);
$num		= str_ireplace("%MM", date("m", $invoice_date), $num);

$num		= str_ireplace("%IIII", sprintf("%1$04d", $year_index), $num);
$num		= str_ireplace("%III", sprintf("%1$03d", $year_index), $num);
$num		= str_ireplace("%II", sprintf("%1$02d", $year_index), $num);
$num		= str_ireplace("%I", sprintf("%d", $year_index), $num);


echo $num;