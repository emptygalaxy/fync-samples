<?php

class APIM
{
	//	Fync prod
	#const REDIRECT_URI	= "https://fync.nl/oauth/";
	#const CLIENT_ID		= "93078550-2f4b-46e8-af9f-a0477ddeb55f";
	#const API			= "https://api.rabobank.nl/openapi/";
	
	//	Oauthdemo
	#const REDIRECT_URI	= "https://www.google.nl/";
	#const CLIENT_ID		= "3a85803a-09c3-4da5-8c81-140f02316796";
	#const CLIENT_ID		= "00776d64-f9ef-464e-bcac-2c0bedf95de1";
	
	//	Production
	
	//	Acceptance fync
	const REDIRECT_URI	= "https://portal.acceptance.fync.nl/oauth";
	const CLIENT_ID		= "d53c69a2-722d-4f82-8b49-3d7a62c2be9b";
	const API			= "https://api-acpt.portaal.rabobank.nl/openapi/";
	
	
	const SCOPE			= "Transacties";
	
	const PUB_KEY		= "./encr/mykey.pub";
	const PRI_KEY		= "./encr/mykey.pem";
	
	public static function requestConsent()
	{
		
		$params	= [
			"response_type"	=> "code",
			"redirect_uri"	=> self::REDIRECT_URI,
			"scope"			=> self::SCOPE,
			"client_id"		=> self::CLIENT_ID,
			"qsl_reqcnt"	=> 1
		];
		
		$url	= self::API . "oauthdemo/oauth2/authorize" . "?" . http_build_query($params);
		
		return $url;
	}
	
	public static function handleCode($code)
	{
		$url	= self::API . "oauthdemo/oauth2/token";
		
		$params	= [
			"grant_type"	=> "authorization_code",
			"redirect_uri"	=> self::REDIRECT_URI,
			"client_id"		=> self::CLIENT_ID,
			"code"			=> $code
		];
		
		$result	= self::call($url, $params);
		
		return $result;
	}
	
	public static function handleRefreshToken($refresh_token)
	{
		$url	= self::API . "oauthdemo/oauth2/token";
		
		$params	= [
			"grant_type"	=> "refresh_token",
			"client_id"		=> self::CLIENT_ID,
			"refresh_token"	=> $refresh_token
		];
		
		$result	= self::call($url, $params);
		
		return $result;
	}
	
	
	
	
	
	public static function getAccounts($token)
	{
		$url	= self::API . "accounts";
		
		$result	= self::call($url, $params=null, $token);
		return $result;
	}
	
	public static function getTest($grossYearlyIncome, $grossYearlyIncomePartner, $token)
	{
		$url	= self::API . "oauthdemo/apim_oauth?" . http_build_query(["grossYearlyIncome"=>$grossYearlyIncome, "grossYearlyIncomePartner"=>$grossYearlyIncomePartner]);
		$result	= self::call($url, $params=null, $token);
	}
	
	
	public static function getTransactions($accountId, $token)
	{
		$url	= self::API . "accounts/transactions?id=" . $accountId;
		
		$result	= self::call($url, $params=NULL, $token);
		return $result;
	}
	
	private static function call($url, $data, $token=NULL)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		
		$tmpfname = dirname(__FILE__).'/'.$_COOKIE['PHPSESSID'].'.txt';
		curl_setopt( $ch, CURLOPT_COOKIEJAR,  $tmpfname);
        curl_setopt( $ch, CURLOPT_COOKIEFILE, $tmpfname);
		
		
		if(isset($data))
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		
		$headers	= [
			"cache-control: no-cache",
			"X-IBM-Client-Id: " . self::CLIENT_ID,
			#"Accept: application/json",
			"Accept-Encoding: gzip",
			"User-Agent: PostmanRuntime/3.0.1",
			"Accept: */*",
			"Connection: close"
		];
		
		
		if($token)
			$headers[]	= "Authorization: Bearer " . $token;
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		if(false)
		{
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
		}
		
		
		if(true)
		{
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
			curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
		}
		
		
		
		$response	= curl_exec($ch);
		
		
		if(gzdecode($response))
			$response	= gzdecode($response);
		
		$result		= json_decode($response);
		
		return $result;
	}
	
	
	public static function encrypt($plaintext)
	{
		$fp	= fopen(self::PUB_KEY, "r");
		$pub_key	= fread($fp,8192);
		fclose($fp);
		
		openssl_get_publickey($pub_key);
		openssl_public_encrypt($plaintext, $crypttext, $pub_key);
		
		$encoded	= base64_encode($crypttext);
		return $encoded;
	}

	public static function decrypt($encryptedext)
	{
		$fp	= fopen(self::PRI_KEY, "r");
		$priv_key	= fread($fp,8192);
		fclose($fp);
		
		$private_key	= openssl_get_privatekey($priv_key);
		$decoded		= base64_decode($encryptedext);
		openssl_private_decrypt($decoded, $decrypted, $private_key);
		
		return $decrypted;
	}
	
}